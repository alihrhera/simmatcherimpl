package com.test_task.supermatcher.matcher

import com.test_task.supermatcher.data.Candidate

import junit.framework.TestCase
import org.junit.Assert
import org.junit.Test

class SimMatcherImplTest {
    private val simMatcherImpl = SimMatcherImpl()


    @Test
    fun `simMatcherImpl compareCandidates candidate name  empty check `() {
        Assert.assertEquals(
            -1,
            simMatcherImpl.compareCandidates(
                candidate = Candidate(1, "", "tesla"),
                matcher = Candidate(2, "Carl Friedrich", "Gauss")
            )
        )
    }

    @Test
    fun `simMatcherImpl compareCandidates matcher name empty check `() {
        Assert.assertEquals(
            -1,
            simMatcherImpl.compareCandidates(
                candidate = Candidate(1, "nikola", "tesla"), matcher = Candidate(2, "", "Gauss")
            )
        )
    }

    @Test
    fun `simMatcherImpl compareCandidates candidate surname empty check `() {
        Assert.assertEquals(
            -1,
            simMatcherImpl.compareCandidates(
                candidate = Candidate(1, "nikola", ""),
                matcher = Candidate(2, "Carl Friedrich", "Gauss")
            )
        )
    }

    @Test
    fun `simMatcherImpl compareCandidates matcher surname empty check `() {
        Assert.assertEquals(
            -1,
            simMatcherImpl.compareCandidates(
                candidate = Candidate(1, "nikola ", "tesla"),
                matcher = Candidate(2, "Carl Friedrich", "")
            )
        )
    }


    @Test
    fun `simMatcherImpl compareCandidates done and accurate `() {
        Assert.assertEquals(
            62,
            simMatcherImpl.compareCandidates(
                candidate = Candidate(1, "nikola ", "tesla"),
                matcher = Candidate(2, "Carl Friedrich", "Gauss")
            )
        )
    }

}