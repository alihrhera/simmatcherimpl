package com.test_task.supermatcher.ui.main

import androidx.lifecycle.ViewModel
import com.test_task.supermatcher.data.Candidate
import com.test_task.supermatcher.data.SimDB
import com.test_task.supermatcher.matcher.SimMatcherImpl
import kotlinx.android.synthetic.main.main_fragment.*

class MainViewModel : ViewModel() {
    private val database = SimDB()
    private val matcher = SimMatcherImpl()

    fun matchCandidateAgainstOthers(
        candidate: Candidate,
        mainFragment: MainFragment
    ) {
        val candidates = database.getAllCandidatesFromDatabase()
        val  candidatesMatchResult= mutableListOf<String>()

        for (it in candidates) {
            val matchVal = matcher.compareCandidates(candidate, it)
            candidatesMatchResult.add("Matching Name:${it.name} ${it.surname} value=$matchVal")
        }

        mainFragment.message.text = ("Matching done  Values is :- ")
        for (value in candidatesMatchResult){
            mainFragment.message.append("\n $value")

        }

    }
}