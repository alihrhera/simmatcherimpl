package com.test_task.supermatcher.matcher

import com.test_task.supermatcher.data.Candidate

class SimMatcherImpl : SimMatcher {

    /**
     * This method should return a positive comparison score between candidates.
     *
     * The score is calculated as the difference between the candidate's 'signature'.
     *
     * A ‘signature’ is calculated by summing the values of each character in a candidate’s full name,
     * where a=1, b=2, c=3, and so on. Don't worry about accented characters (á, é), they don't appear.
     *
     * E.g., If a candidate is called “Thales Lima”, his signature will be 100.
     * When compared to a candidate called “Fabio Tuzza” the returned comparison will be 27.
     *
     * The current implementation is a mock version of a matcher.
     *
     * Tasks:
     * - Implement a real matcher based on the description above
     * - Write tests
     */
    private val listOfChar = ('a'..'z').toMutableList()

    override fun compareCandidates(candidate: Candidate, matcher: Candidate): Int {

        if (candidate.name.isEmpty() || candidate.surname.isEmpty()) {
            return -1
        }

        if (matcher.name.isEmpty() || matcher.surname.isEmpty()) {
            return -1
        }

        val candidateNameValue = getNameValue("${candidate.name}${candidate.surname}")

        val matcherNameValue = getNameValue("${matcher.name}${matcher.surname}")

        val matchingValue = candidateNameValue - matcherNameValue


        return if (matchingValue > 0) matchingValue else {
            matchingValue * -1
        }
    }


    private fun getNameValue(name: String): Int {
        var nameValue = 0
        for (c in name) {
            nameValue += ((listOfChar.indexOf(c.toLowerCase()) + 1))
        }
        return nameValue
    }


}