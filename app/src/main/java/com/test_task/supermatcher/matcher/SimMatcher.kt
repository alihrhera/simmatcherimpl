package com.test_task.supermatcher.matcher

import com.test_task.supermatcher.data.Candidate

interface SimMatcher {
    fun compareCandidates(candidate: Candidate, matcher: Candidate): Int
}